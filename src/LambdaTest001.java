//Expresiones lambda sin parametros
public class LambdaTest001{
    public static void main (String[] args){
        //Expresion lambda ==> representa un objeto de una interfaz funcional
        Functiontest ft = () -> System.out.println("Hola mundo");
        //Implementación del método abstracto saludar de la interfaz funcional
        
        //ft.saludar();

        LambdaTest001 objeto = new LambdaTest001();
        objeto.miMetodo(ft);
    }
    public void miMetodo (Functiontest parametro){
        parametro.saludar();
    }
}