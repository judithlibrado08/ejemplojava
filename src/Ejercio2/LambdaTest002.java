package Ejercio2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author judit
 */
public class LambdaTest002 {
    public static void main (String [] args){
        //Expresion lambda ==> representa un objeto de una interfaz
        Operaciones op = (num1, num2) -> System.out.println(num1 + num2);
        op.imprimeSuma(5, 10);

        LambdaTest002 objeto = new LambdaTest002();
        //objeto.miMetodo(op, 10,10);
        objeto.miMetodo1((num1, num2) -> System.out.println(num1 - num1), 20,10);
        objeto.miMetodo1((num1, num2) -> System.out.println(num1 * num1), 20,10);
    }
    public void miMetodo1(Operaciones op, int num1, int num2){
        op.imprimeSuma(num1, num2);
    }
}
